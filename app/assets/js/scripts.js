/*!
 * fastshell
 * Fiercely quick and opinionated front-ends
 * https://HosseinKarami.github.io/fastshell
 * @author Hossein Karami
 * @version 1.0.5
 * Copyright 2020. MIT licensed.
 */
(function ($, window, document, undefined) {

  'use strict';

  $(function () {
    
  //Smooth Scroll
  $(document).on('click', 'a[href^="#"]', function (event) {
      event.preventDefault();

      $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top
      }, 500);
  });

  // Contact from 
    $('.report-dl').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.modal-cf#contact-form').addClass('show');
    });






// LOCAL - MAP CODE
var mapHeight = $('.map--world').outerHeight(true);
$('.country-info').css('min-height', mapHeight);

var $frameBlack = ('.page-transition__white');
var $frameRed = ('.page-transition__grey');
var $mapHome = ('.map--world');
const $hiddenContent = ('.legend');
var legendHeight = $('.inner--reveal').outerHeight(true);


$(window).resize(function() {
  $('.map-section').css('height', 'auto').height();
});
$(window).trigger('resize');

// Display Map Country Content 
$('.country').click(function(e) {
  e.preventDefault();
  var country = $(this).attr('data-country');
  var countryData = $("#" + country);
  var mapSection = $('.map-section').outerHeight(true);

  $('.map-section').css('height', mapSection);

  var tltransition = new TimelineMax({onComplete:resizeMapSection})
  .fromTo($frameRed , 1.5, {scaleX: 0},{scaleX: 1, transformOrigin:'left', ease: Power4.easeInOut, force3D:true})
  .fromTo($frameBlack , 1.5, {scaleX: 0},{scaleX: 1, transformOrigin:'left', ease: Power4.easeInOut, force3D:true}, -0.3)
  .to($mapHome, .1, {height:0, autoAlpha:0})
  .set($frameBlack, {scaleX:0}, "-=0.3")
  .set(countryData, { className: "+=show"},"-=0.2")
  .to($frameRed , 1.5, {scaleX: 0, transformOrigin:'right', ease: Power4.easeInOut}, "-=0.35")
  .from( '.country-text h2',0.5, {autoAlpha:0, y: 100}, "-=0.75")
  .from( '.country-text .country-bullet',0.5, {autoAlpha:0,y: 100}, "-=0.5")
  .from( '.country-text a.dark-ghost',0.5, {autoAlpha:0,y: 100}, "-=0.6")
  .from( '.back-to-map',0.5, {autoAlpha:0,y: -100})
  .fromTo($hiddenContent, 0.5, { height: 0}, { height:legendHeight},"-=0.6")
  


  function resizeMapSection() {
    resizeMapContainer();
    var tltransition = new TimelineMax()
    .fromTo('.inner--reveal', 1, { autoAlpha: 0}, { autoAlpha:1},"+=0.5")

  }
});



// Country Map Back Button
$('.back-to-map').click(function(e) {

  var mapSection = $('.map-section').outerHeight(true);
  $('.map-section').css('height', mapSection);

  var el = $('.map-section');
  $(el).css('height', mapSection).height();
  var newLegendHeight = $('.inner--reveal').outerHeight(true);

  mapZoom.reset();

  var tltransition = new TimelineMax({onComplete:resizeMapSection})
  .fromTo($hiddenContent, 0.5, { height: newLegendHeight }, { height:0})
  .fromTo($frameRed , 1.5, {scaleX: 0},{scaleX: 1, transformOrigin:'right', ease: Power4.easeInOut, force3D:true}, -0.1)
  .fromTo($frameBlack , 1.5, {scaleX: 0},{scaleX: 1, transformOrigin:'right', ease: Power4.easeInOut, force3D:true}, -0.3, "-=1")
  .set($frameBlack, {scaleX:0})
  .to($mapHome, 0.1, {height:"100%", autoAlpha:1})
  .fromTo('.inner--reveal', 0.1, { autoAlpha: 1}, { autoAlpha:0})
  .set('.country-info', { className: "-=show"})
  .to($frameRed , 1.5, {scaleX: 0, transformOrigin:'left', ease: Power4.easeInOut}, "-=0.4")

  function resizeMapSection() {
    resizeMapContainer();
    
  }
});


function resizeMapContainer() {
  var el = $('.map-section'),
    curHeight = el.height(),
    autoHeight = el.css('height', 'auto').height();
    el.height(curHeight).animate({height: autoHeight}, 1000);
}

var mapZoom = svgPanZoom('#world-map', {
  controlIconsEnabled: false,
  preventMouseEventsDefault: false,
  mouseWheelZoomEnabled: false

});


$(window).resize(function(){
  mapZoom.resize();
  mapZoom.fit();
  mapZoom.center();
});

$('#zoom--in').click(function(e) {
  e.preventDefault();

  mapZoom.zoomIn();

});

$('#zoom--out').click(function(e) {
  e.preventDefault();

  mapZoom.zoomOut();

});

$('#reset').click(function(e) {
  e.preventDefault();

  mapZoom.reset();

});


      // ELOQUA CODE 
  var qs = (function(a) { if (a == "") return {}; var b = {}; for (var i = 0; i < a.length; ++i) { var p=a[i].split('=', 2); if (p.length == 1) b[p[0]] = ""; else b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " ")); } return b; })(window.location.search.substr(1).split('&'));

if (document.documentMode < 9) {
  document.body.firstChild.insertAdjacentHTML('beforebegin', '<center><div style="max-width: 600px; background: pink; font-weight: bold; padding: 10px 0px;">We detected you are using an outdated browser that will prevent you from accessing certain features. Please update or use a different browser to improve your browsing experience.</div\></center>');
  document.forms[0].disabled = true;
}

$('#referrer_url').val(window.location.origin + window.location.pathname);
$('#source').val(qs["utm_source"] || "");
$('#campaign').val(qs["utm_campaign"] || "");
$('#content').val(qs["utm_content"] || "");
$('#medium').val(qs["utm_medium"] || "");
$('#term').val(qs["utm_term"] || "");
$('#form_name').val($('#elqFormName').val());
$('#eloqua_campaign_id').val($('#elqCampaignId').val());


$("form").submit(function(){
  var a=[];$('input:checked[name="solution_category_cb"]').each(function(){a.push($(this).val());}); $('#solution_category').val(a.join(','));
});

$( document ).ready(function() {
  var ruleSet = { required : function(elem) { return $(elem).filter(':checked').length == 0 } }
  var messageSet = { required: "Please make at least one selection." };
  $('#elq-form').validate({ 
    errorPlacement : function (error, element) {
      if($(element).prop('type')==='checkbox')
        error.insertAfter($('.request-options'));
      else
        error.insertAfter($(element));
    }
    , rules : {
      solution_category_cb: ruleSet
    }
    , messages : {
      solution_category_cb : messageSet
    }
  });   
});



  }); //end

})(jQuery, window, document);
